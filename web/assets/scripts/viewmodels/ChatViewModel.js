function messageViewModel(nick, message) {
	var self = this;

	self.Nick = ko.observable(nick);
	self.Message = ko.observable(message);

	self.NickForDisplay = ko.computed(function () {
		return self.Nick() + ':';
	}, self);
}

function chatViewModel() {
	var self = this;

	self.Nick = ko.observable();

	self.Nick.subscribe(function (value) {
		self.socket.emit('set nick', value);
	});
	
	self.Messages = ko.observableArray();

	self.sendMessage = function () {
		var msg = new messageViewModel(this.Nick(), $('#message').val());
		self.Messages.push(msg);
		self.socket.emit('msg', ko.toJSON(msg));
		$('#message').val('');
	};

	self.socket = io.connect('http://localhost');

	self.socket.on('msg', function (data) {
		data = JSON.parse(data);
		var msg = new messageViewModel(data.Nick, data.Message);
		self.Messages.push(msg);
	});
};

$(function () {
	var vm = new chatViewModel();
	ko.applyBindings(vm);

	$('#change-nick').modal('show');


});