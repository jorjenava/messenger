var config = require('./config'),
	connect = require('connect'),
	server = connect.createServer(
    	connect.static(config.webroot)
	).listen(config.port),
	io = require('socket.io').listen(server);


io.sockets.on('connection', function (socket) {
  
  socket.on('set nick', function (name) {
  	console.log('Debug: ' + name);
  	socket.set('nickname', name, null);
  });

  socket.on('msg', function(data) {
  	socket.broadcast.emit('msg', data);
  	console.log(data);
  });

});